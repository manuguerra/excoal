## First time running ExCoAl

In R or RStudio (the use of the latter will be assumed from now on), install and load the package:

	> install_bitbucket("manuguerra/excoal")
	> library(ExCoAl)
	
Now we need to create and setwd() into the project folder, e.g., if we are developing the MATH101 course::

	> dir.create("MATH101")
	> setwd("MATH101")
	
Before running the package, we need to create two files: one text file describing the course outline and one R file where we will create the question functions.

	> file.create("outline.txt")
	> file.create("lib_qu.R")

We can now run ExCoAl:

	> open_project(outline = "outline.txt", lib_qu = "lib_qu.R", development = T)
	
As the outline.txt file is empty, we can't do anything.  
Type "0" [return] to exit the menu, then open and edit the outline.txt file. 

*More to come...*

## Exporting a project to Moodle

First load the ExCoAl package:

	> library(ExCoAl)
	
Assuming you are in the folder of the course you want to export, you should see two files, the outline file and the question library R file. Assuming they are named outline.txt and lib_qu.R, run:

	> open_project(outline = "outline.txt", lib_qu = "lib_qu.R")
	
Type "1" [return] and answer the questions. The Moodle XML file will be created in the working folder, ready to be imported in the Question Bank of Moodle.